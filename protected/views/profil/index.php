<div class="row">
  <div class="col-md-5 f-margin-2 f-padding-2 f-border-lightgrey alert alert-info">
    <div class="pull-right image">
              <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
            <br/><br/>
         </div>

    <h3>Profil Anda</h3>
    <table class="table">
      <tr>
        <td>Nama</td>
        <td>:</td>
        <td><?php echo $dataUser['nama'] ?></td>
      </tr>
      <tr>
        <td>NIP</td>
        <td>:</td>
        <td><?php echo $dataUser['nip'] ?></td>
      </tr>
      <tr>
        <td>Unit Bagian</td>
        <td>:</td>
        <td><?php echo $dataUser['unitbagian'] ?></td>
      </tr>
      <tr>
        <td>Level User</td>
        <td>:</td>
        <td><?php 
          echo $dataUser['level'];
            
                if ($dataUser['level']=='0')
                  {
                    $level="(User / review / guest)";
                    echo $level;
                  } 
                     if ($dataUser['level']=='1')
                  {
                    $level=" (Manager E-Kegiatan)";
                    echo $level;
                  } 

                    if ($dataUser['level']=='2')
                  {
                    $level=" (Admin Sistem) ";
                    echo $level;
                  } 
             


                

        ?></td>
      </tr>
      <tr>
        <td>Password</td>
        <td>:</td>
        <td><?php 
          echo md5($dataUser['password']);
        ?></td>
      </tr>
    </table>      
  </div>
  <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/profil/UpdateProfil/">
  <div class="col-md-7">
      <div class="box box-info">
        <div class="box-header">
          
        </div>
        <div class="box-body">
         <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" value="<?php echo $dataUser['nama'] ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>NIP</label>
            <input type="text" name="nip" placeholder="Judul" value="<?php echo $dataUser['nip'] ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="tlp" placeholder="Judul" value="<?php echo $dataUser['tlp'] ?>" class="form-control">
          </div>
          <div class="form-group">
            <label>Unit Bagian</label>
            <input type="text" name="unitbagian" placeholder="Judul" value="<?php echo $dataUser['unitbagian'] ?>" class="form-control">
          </div>

          <div class="form-group">
            <label>User Name</label>
            <input type="text" name="username" value="<?php echo $dataUser['username'] ?>" class="form-control" >
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="text" name="password" placeholder="***********" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Level</label>
           <input type="text" name="level" class="form-control" placeholder="<?php echo $level?>" disabled>
          </div>
          
          <p>Perhatikan data yang anda inputkan kembali</p>
          <a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn btn-danger">Kembali</a>
          <input type="hidden" name="id" value="<?php echo $dataUser['id'] ?>">
            <input type="submit" class="btn btn-info" value="Simpan">
        </div>
      </div>
  </div>
  </form>
</div>
    
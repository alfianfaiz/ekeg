<table class="table table-condensed">
	<tr>
		<td>Kode</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->kode_kegiatan ?></td>
	</tr>
	<tr>
		<td>Nama Program</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->nama_kegiatan ?></td>
	</tr>
	<tr>
		<td>Target</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->target ?></td>
	</tr>

	<tr>
		<td>Volume</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->volume ?></td>
	</tr>

	<tr>
		<td>Harga Satuan</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->harga_satuan ?></td>
	</tr>

	<tr>
		<td>Satuan</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->satuan ?></td>
	</tr>
	<tr>
		<td>Sumber Dana</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->sumber_dana ?></td>
	</tr>

	<tr>
		<td>Penanggung Jawab</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->penanggung_jawab ?></td>
	</tr>

	<tr>
		<td>Waktu Update</td>
		<td>:</td>
		<td><?php echo $recoKegiatan->waktu_update ?></td>
	</tr>
</table>
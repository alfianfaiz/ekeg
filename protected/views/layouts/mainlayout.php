<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>E-Kegiatan PPS UNNES | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.5 -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Theme style -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/css/AdminLTE.min.css">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
         folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/css/skins/_all-skins.min.css">
    <!-- iCheck -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/iCheck/flat/blue.css">
    <!-- Morris chart -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/morris/morris.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.css">
    <!-- Date Picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/datepicker/datepicker3.css">
    <!-- Daterange picker -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/daterangepicker/daterangepicker-bs3.css">
    <!-- bootstrap wysihtml5 - text editor -->
    <link rel="stylesheet" href="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
    <!-- jQuery 2.1.4 -->
    <script  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/js/jquery-2.1.4.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/input-mask/jquery.inputmask.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/input-mask/jquery.inputmask.extensions.js"></script>
    <!-- jQuery UI 1.11.4 -->
   
    
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body class="hold-transition skin-green sidebar-mini">
    <div class="wrapper">

      <header class="main-header">
        <!-- Logo -->
        <a href="index2.html" class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>A</b>LT</span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>E</b>-Kegiatan</span>
        </a>
        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <!-- User Account: style can be found in dropdown.less -->
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/avatar04.png" class="user-image" alt="User Image">
                  <span class="hidden-xs"><?php echo Yii::app()->user->name; ?></span>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  <li class="user-header">
                    <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/avatar04.png" class="img-circle" alt="User Image">
                    <p>
                      <?php echo Yii::app()->user->name; ?>
                      <small>
                          <?php
                            if(Yii::app()->user->isMember())
                              { 
                                 echo "As Reviewer E-Kegiatan";
                              }
                            elseif(Yii::app()->user->isAdmin())
                              { 
                                 echo "As System Administrator";
                              }
                            elseif(Yii::app()->user->isManager())
                              { 
                                 echo "As Manager E-Kegiatan";
                              }
                          ?>
                      </small>
                    </p>
                  </li>
                                  <!-- Menu Footer-->
                  <li class="user-footer">
                    <div class="pull-left">
                      <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/profil">
                        <input type="hidden" value="<?php echo Yii::app()->user->getId(); ?>" name="id">
                        <button type="submit" class="btn btn-default btn-flat">Profil</button>
                      </form>
                    </div>
                    <div class="pull-right">
                      <a href="<?php echo Yii::app()->request->baseUrl ?>/login/logout" class="btn btn-default btn-flat">Sign out</a>
                    </div>
                  </li>
                </ul>
              </li>
              <!-- Control Sidebar Toggle Button -->
              <li>
                <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
              </li>
            </ul>
          </div>
        </nav>
      </header>
      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
          <!-- Sidebar user panel -->
          <div class="user-panel">
            <div class="pull-left image">
              <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/avatar04.png" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
              <p>   <?php echo Yii::app()->user->name; ?>  </p>
              <a href="#"><i class="fa fa-circle text-success"></i>
              <?php
                if(Yii::app()->user->isMember())
                  { 
                     echo "As Reviewer";
                  }
                elseif(Yii::app()->user->isAdmin())
                  { 
                     echo "As Administrator";
                  }
                elseif(Yii::app()->user->isManager())
                  { 
                     echo "As Manager E-Kegiatan";
                  }
              ?>
             </a>
            </div>
          </div>
          <!-- sidebar menu: : style can be found in sidebar.less -->
          <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>
            <li class="treeview">
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/home">
                <i class="fa fa-dashboard"></i> <span>Dashboard</span>
              </a>
            </li>
              <?php if(Yii::app()->user->isAdmin())
                  { ?>
                     

           <li class="treeview">
              <a href="">
                <i class="glyphicon glyphicon-log-out"></i> 
                <span>Recover Data</span><i class="fa fa-angle-left pull-right"></i>
                <ul class="treeview-menu">
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/recoverdata/program"><i class="fa fa-circle-o"></i> Recover Program</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/recoverdata/layanan"><i class="fa fa-circle-o"></i> Recover Layanan</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/recoverdata/kegiatan"><i class="fa fa-circle-o"></i> Recover Kegiatan</a></li>
                    <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/recoverdata/realisasi"><i class="fa fa-circle-o"></i> Recover realisasi</a></li>
                </ul>
              </a>
            </li>
            <li class="active treeview">
                <a href="<?php echo Yii::app()->request->baseUrl; ?>/manageuser">
                  <i class="glyphicon glyphicon-list-alt"></i>
                    <span>Kelola User</span>
                </a>
           </li>


                  <?php } ?>



          <?php if(Yii::app()->user->isManager())
                  { ?>
            <li class="treeview">
              <a href="#">
                <i class="glyphicon glyphicon-list-alt"></i>
                <span>Kelola POK</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram"><i class="fa fa-circle-o"></i> Kelola Program</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/kelolajadwal"><i class="fa fa-circle-o"></i> Kelola Jadwal</a></li>
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/aturanggaran"><i class="fa fa-circle-o"></i> Kelola Anggaran</a></li>
              </ul>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa  fa-calendar"></i>
                <span>Lihat Perencanaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpokbulananV2"><i class="fa fa-circle-o"></i> Rencana Bulanan</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpoktriwulan"><i class="fa fa-circle-o"></i> Rencana TriBulanan</a></li>
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpoksemester"><i class="fa fa-circle-o"></i> Rencana Semesteran</a></li>
              </ul>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/kelengkapan">
                <i class="fa fa-dashboard"></i> <span>Kelola Kelengkapan</span>
              </a>
            </li>
            <li class="treeview">
              <a href="#">
                <i class="fa fa-cogs"></i>
                <span>Kelola Pelaksanaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/inputrealisasi"><i class="fa fa-circle-o"></i> Input Realisasi</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/inputrealisasi/daftarrealisasi"><i class="fa fa-circle-o"></i> Lihat Daftar Realisasi</a></li>
              </ul>
              </a>
            </li>
            
           <li class="treeview">
              <a href="#">
                <i class="fa fa-calendar-check-o"></i>
                <span>Rekap Pelaksanaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rekapseluruh/openData"><i class="fa fa-circle-o"></i> Rekap Bulanan</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rekapbulanan"><i class="fa fa-circle-o"></i> Rekap Bulanan</a></li>
                  <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Rekap Triwulanan</a></li>
                <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Rekap Semesteran</a></li>
              </ul>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/downloadrekap">
                <i class="glyphicon glyphicon-download-alt"></i> <span>Download</span>
              </a>
            </li>
                     

          


                  <?php } ?>



      <?php if(Yii::app()->user->isMember())
           { ?>
             
            <li class="treeview">
              <a href="#">
                <i class="fa  fa-calendar"></i>
                <span>Lihat Perencanaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpokbulananV2"><i class="fa fa-circle-o"></i> Rencana Bulanan</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpoktriwulan"><i class="fa fa-circle-o"></i> Rencana TriBulanan</a></li>
                <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/lihatpoksemester"><i class="fa fa-circle-o"></i> Rencana Semesteran</a></li>
              </ul>
              </a>
            </li>
            
            
            
           <li class="treeview">
              <a href="#">
                <i class="fa fa-calendar-check-o"></i>
                <span>Rekap Pelaksanaan</span><i class="fa fa-angle-left pull-right"></i>
                <!-- <span class="label label-primary pull-right">4</span> -->
                <ul class="treeview-menu">
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rekapseluruh/openData"><i class="fa fa-circle-o"></i> Rekap Bulanan</a></li>
                  <li><a href="<?php echo Yii::app()->request->baseUrl; ?>/rekapbulanan"><i class="fa fa-circle-o"></i> Rekap Bulanan</a></li>
                  <li><a href="pages/charts/morris.html"><i class="fa fa-circle-o"></i> Rekap Triwulanan</a></li>
                <li><a href="pages/charts/flot.html"><i class="fa fa-circle-o"></i> Rekap Semesteran</a></li>
              </ul>
              </a>
            </li>
            <li class="treeview">
              <a href="<?php echo Yii::app()->request->baseUrl; ?>/downloadrekap">
                <i class="glyphicon glyphicon-download-alt"></i> <span>Download</span>
              </a>
            </li>
                     

          


                  <?php } ?>

            
            
            <li class="treeview">
              <a href="<?php echo Yii::app()->request->baseUrl ?>/login/logout">
                <i class="glyphicon glyphicon-log-out"></i> <span>Logout Me</span>
              </a>
            </li>
          </ul>
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="alertbox">
          <div class="container-fluid">
            <!-- <div class="col-md-12">
              <div class="callout callout-info">
                <h4>Tip!</h4>
                <p>Add the layout-top-nav class to the body tag to get this layout. This feature can also be used with a sidebar! So use this class if you want to remove the custom dropdown menus from the navbar and use regular links instead.</p>
              </div>
            </div> -->
          </div>
        </section>
        <section class="content-header">
          <h1>
            Dashboard
            <small>Control panel</small>
          </h1>
          <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li class="active">Dashboard</li>
          </ol>
        </section>

        <!-- Main content -->
        <section class="content">
          <!-- Small boxes (Stat box) -->
          <div class="container-fluid">
            <?php echo $content ?>
          </div>
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->
      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 2.3.0
        </div>
        <strong>Copyright &copy; 2014-2015 <a href="http://almsaeedstudio.com">Almsaeed Studio</a>.</strong> All rights reserved.
      </footer>

      <!-- Control Sidebar -->
      <aside class="control-sidebar control-sidebar-dark">
        <!-- Create the tabs -->
        <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
          <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
          <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
          <!-- Home tab content -->
          <div class="tab-pane" id="control-sidebar-home-tab">
            <h3 class="control-sidebar-heading">Recent Activity</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-birthday-cake bg-red"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
                    <p>Will be 23 on April 24th</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-user bg-yellow"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>
                    <p>New phone +1(800)555-1234</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>
                    <p>nora@example.com</p>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <i class="menu-icon fa fa-file-code-o bg-green"></i>
                  <div class="menu-info">
                    <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>
                    <p>Execution time 5 seconds</p>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

            <h3 class="control-sidebar-heading">Tasks Progress</h3>
            <ul class="control-sidebar-menu">
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Custom Template Design
                    <span class="label label-danger pull-right">70%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Update Resume
                    <span class="label label-success pull-right">95%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Laravel Integration
                    <span class="label label-warning pull-right">50%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                  </div>
                </a>
              </li>
              <li>
                <a href="javascript::;">
                  <h4 class="control-sidebar-subheading">
                    Back End Framework
                    <span class="label label-primary pull-right">68%</span>
                  </h4>
                  <div class="progress progress-xxs">
                    <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                  </div>
                </a>
              </li>
            </ul><!-- /.control-sidebar-menu -->

          </div><!-- /.tab-pane -->
          <!-- Stats tab content -->
          <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
          <!-- Settings tab content -->
          <div class="tab-pane" id="control-sidebar-settings-tab">
            <form method="post">
              <h3 class="control-sidebar-heading">General Settings</h3>
              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Report panel usage
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Some information about this general settings option
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Allow mail redirect
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Other sets of options are available
                </p>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Expose author name in posts
                  <input type="checkbox" class="pull-right" checked>
                </label>
                <p>
                  Allow the user to show his name in blog posts
                </p>
              </div><!-- /.form-group -->

              <h3 class="control-sidebar-heading">Chat Settings</h3>

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Show me as online
                  <input type="checkbox" class="pull-right" checked>
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Turn off notifications
                  <input type="checkbox" class="pull-right">
                </label>
              </div><!-- /.form-group -->

              <div class="form-group">
                <label class="control-sidebar-subheading">
                  Delete chat history
                  <a href="javascript::;" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
                </label>
              </div><!-- /.form-group -->
            </form>
          </div><!-- /.tab-pane -->
        </div>
      </aside><!-- /.control-sidebar -->
      <!-- Add the sidebar's background. This div must be placed
           immediately after the control sidebar -->
      <div class="control-sidebar-bg"></div>
    </div><!-- ./wrapper -->

    
    <!-- Bootstrap 3.3.5 -->
    <script  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/bootstrap/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/morris/morris.min.js"></script>
    <!-- Sparkline -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/sparkline/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/knob/jquery.knob.js"></script>
    <!-- daterangepicker -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.2/moment.min.js"></script>
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/datepicker/bootstrap-datepicker.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/slimScroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/plugins/fastclick/fastclick.min.js"></script>
    <!-- AdminLTE App -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/js/app.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/js/pages/dashboard.js"></script>
    <!-- AdminLTE for demo purposes -->
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/js/demo.js"></script>
  </body>
</html>

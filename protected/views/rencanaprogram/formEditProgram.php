<div class="row">
	<div class="col-md-4 f-margin-2 f-padding-2 f-border-lightgrey alert alert-warning">
		<h3>Data Tersimpan</h3>
		<table class="table">
			<tr>
				<td>Kode Program</td>
				<td>:</td>
				<td><?php echo $dataProgram['kode_program'] ?></td>
			</tr>
			<tr>
				<td>Tahun Anggaran</td>
				<td>:</td>
				<td><?php echo $dataProgram['nama_program'] ?></td>
			</tr>
			<tr>
				<td>Tahun Anggaran</td>
				<td>:</td>
				<td><?php echo $dataProgram['tahun_anggaran'] ?></td>
			</tr>
		</table>			
	</div>
	<form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/UpdateProgram/">
	<div class="col-md-7">
			<div class="box box-danger">
				<div class="box-header">
					
				</div>
				<div class="box-body">
					<div class="form-group">
						<label>Kode</label>
						<input type="text" name="kodeTP" value="<?php echo $dataProgram['kode_program'] ?>" class="form-control" >
					</div>
					<div class="form-group">
						<label>Nama Program</label>
						<input type="text" name="namaTP" placeholder="Judul" value="<?php echo $dataProgram['nama_program'] ?>" class="form-control">
					</div>
					<div class="form-group">
						<label>Tahun Anggaran</label>
						<select name="tahunTP" class="form-control">
							<?php AlatUmum::activeOptListYears("2016") ?>
						</select>
					</div>
					<p>Perhatikan data yang anda inputkan kembali</p>
					<a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn">Kembali</a>
					<input type="hidden" name="id" value="<?php echo $dataProgram['id'] ?>">
				    <input type="submit" class="btn btn-primary" value="Simpan">
				</div>
			</div>
	</div>
	</form>
</div>
		
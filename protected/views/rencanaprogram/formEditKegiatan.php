<div class="row">
	<div class="col-md-4 f-margin-2 f-padding-2 f-border-lightgrey alert alert-warning">
		<h3>Data Tersimpan</h3>
		<table class="table">
			<tr>
				<td>Kode Kegiatan</td>
				<td>:</td>
				<td><?php echo $dataKegiatan['kode_kegiatan'] ?></td>
			</tr>
			<tr>
				<td>Nama Kegiatan</td>
				<td>:</td>
				<td><?php echo $dataKegiatan['nama_kegiatan'] ?></td>
			</tr>
		</table>			
	</div>
	<form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/rencanaprogram/UpdateKegiatan/">
	<div class="col-md-7">
		<div class="box box-danger">
			<div class="box-header">
				
			</div>
			<div class="box-body">
				<input type="hidden" name="id" value="<?php echo $dataKegiatan['id'] ?>">
				<input type="hidden" name="id_layanan" value="<?php echo $dataKegiatan['id_layanan'] ?>">
				<div class="form-group">
					<label>Kode</label>
					<input type="text" name="kodeKg" placeholder="Kegiatan" value="<?php echo $dataKegiatan['id_layanan'] ?>" class="form-control">
				</div>
				<div class="form-group">
					<label>Nama Kegiatan</label>
					<input type="text" name="namaKg" placeholder="Judul" class="form-control"  value="<?php echo $dataKegiatan['nama_kegiatan'] ?>" required>
				</div>
			<a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn">Kembali</a>
		    <input type="submit" class="btn btn-primary" value="Simpan">
			</div>
		</div>
	</div>
	</form>
</div>
		
<div class="row">
	<div class="col-md-5 f-margin-2 f-padding-2 f-border-lightgrey alert alert-success">
		<div class="pull-right image">
              <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/user2-160x160.jpg" class="img-circle" alt="User Image">
         		<br/><br/>
         </div>

		<h3>Data Tersimpan</h3>
		<table class="table">
			<tr>
				<td>Nama</td>
				<td>:</td>
				<td><?php echo $dataUser['nama'] ?></td>
			</tr>
			<tr>
				<td>NIP</td>
				<td>:</td>
				<td><?php echo $dataUser['nip'] ?></td>
			</tr>
			<tr>
				<td>Unit Bagian</td>
				<td>:</td>
				<td><?php echo $dataUser['unitbagian'] ?></td>
			</tr>
			<tr>
				<td>Level User</td>
				<td>:</td>
				<td><?php 
					echo $dataUser['level'];
					  
                if ($dataUser['level']=='0')
                  {
                    echo " (User / review / guest) ";
                  } 
                     if ($dataUser['level']=='1')
                  {
                    echo " (Manager E-Kegiatan)";
                  } 

                    if ($dataUser['level']=='2')
                  {
                    echo " (Admin Sistem) ";
                  } 
             


                

				?></td>
			</tr>
			<tr>
				<td>Password</td>
				<td>:</td>
				<td><?php 
					echo $dataUser['password'];
				?></td>
			</tr>
		</table>			
	</div>
	<form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/manageuser/UpdateUser/">
	<div class="col-md-7">
			<div class="box box-warning">
				<div class="box-header">
					
				</div>
				<div class="box-body">
					<div class="form-group">
						<label>User Name</label>
						<input type="text" name="username" value="<?php echo $dataUser['username'] ?>" class="form-control" >
					</div>
					<div class="form-group">
						<label>Password</label>
						<input type="text" name="password" placeholder="Judul" value="<?php echo $dataUser['password'] ?>" class="form-control">
					</div>
					<div class="form-group">
						<label>Level</label>
						<select name="level" class="form-control">
							<option value="0">Reviewer</option>
							<option value="1">Manager</option>
							<option value="2">Administrator</option>
							
						</select>
					</div>
					
					<p>Perhatikan data yang anda inputkan kembali</p>
					<a href="<?php echo Yii::app()->user->returnUrl ?>" class="btn btn-warning">Kembali</a>
					<input type="hidden" name="id" value="<?php echo $dataUser['id'] ?>">
				    <input type="submit" class="btn btn-success" value="Simpan">
				</div>
			</div>
	</div>
	</form>
</div>
		
<div class="row">
  <div class="col-md-5 f-margin-2 f-padding-2 f-border-lightgrey alert alert-success">
    <div class="pull-right image">
              <img  src="<?php echo Yii::app()->request->baseUrl; ?>/assets/themes/adminlte/dist/img/avatar5.png" class="img-circle" alt="User Image">
            <br/><br/>
         </div>

    <h3>Data Tersimpan</h3>
    
  </div>
  <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/manageuser/InsertUser/">
  <div class="col-md-7">
      <div class="box box-info">
        <div class="box-header">
          
        </div>
        <div class="box-body">
         <div class="form-group">
            <label>Nama</label>
            <input type="text" name="nama" placeholder="Nama" class="form-control" >
          </div>
          <div class="form-group">
            <label>NIP</label>
            <input type="text" name="nip" placeholder="NIP" class="form-control">
          </div>
          <div class="form-group">
            <label>Telepon</label>
            <input type="text" name="tlp" placeholder="Telepon"  class="form-control">
          </div>
          <div class="form-group">
            <label>Unit Bagian</label>
            <input type="text" name="unitbagian" placeholder="Unit Bagian"  class="form-control">
          </div>

          <div class="form-group">
            <label>User Name</label>
            <input type="text" name="username" placeholder="Username Login" class="form-control" >
          </div>
          <div class="form-group">
            <label>Password</label>
            <input type="password" name="password" placeholder="" value="" class="form-control">
          </div>
          <div class="form-group">
            <label>Level</label>
          <select name="level" class="form-control">
              <option value="0">Reviewer</option>
              <option value="1">Manager</option>
              <option value="2">Administrator</option>
          </select>
          </div>
          
          <p>Perhatikan data yang anda inputkan kembali</p>
          <a href="" class="btn btn-danger">Kembali</a>
          <input type="hidden" name="id" value="">
            <input type="submit" class="btn btn-info" value="Simpan">
        </div>
      </div>
  </div>
  </form>
</div>
    
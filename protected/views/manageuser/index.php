<div class="row">
          <div class="callout callout-info">
            <h4>Manage User E-Kegiatan</h4>
            <p>Hati-hati dalam melakukan administrasi user, masing-masing level user memiliki hak akses yang berbeda. Admin sistem tetap memegang penuh semua akses.</p>
          </div>



              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Daftar Identitas User</h3>
                  <div class="box-tools">
                  
                  </div>
                </div><!-- /.box-header -->
                <div class="box-body table-responsive no-padding">
                  <table class="table table-hover">
                    <tr>
                      <th>ID</th>
                      <th>Nama</th>
                      <th>NIP</th>
                      <th>Level</th>
                      <th>Unit Bagian</th>
                      <th>Aksi</th>
                    </tr>

        <?php foreach ($result as $key): ?>
            
          
            
            <tr>
              
              <td><?php echo $key['id'] ?></td>
              <td><?php echo $key['nama'] ?></td>
              <td><?php echo $key['nip'] ?></td>

                <?php 
                if ($key['level']=='0')
                  {
                    echo " <td><span class='label label-success'>User / review / guest</span></td> ";
                  } 
                     if ($key['level']=='1')
                  {
                    echo " <td><span class='label label-warning'>Admin E-Kegiatan</span></td> ";
                  } 

                    if ($key['level']=='2')
                  {
                    echo " <td><span class='label label-danger'>Admin Sistem</span></td> ";
                  } 
             


                ?>


              <td><?php echo $key['unitbagian'] ?></td>
            
                   <td>
                      <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/manageuser/editUser/">
                        <input type="hidden" value="<?php echo $key['id']; ?>" name="id">
                        <button type="submit" class="label label-warning"><span class="glyphicon glyphicon-edit"></span></button>
                      </form>
                    </td>
                    <td>
                      <?php if($key['status']=='1') 
                      {
                        $statusValue=0;
                        $classBtn = "glyphicon glyphicon-remove";
                        $status = "Hapus";} 
                        else {
                          $statusValue=1;$classBtn = "glyphicon glyphicon-ok";$status = "Recover";} ?>
                      <form method="post" action="<?php echo Yii::app()->request->baseUrl; ?>/manageuser/HapusUser/<?php echo $key['id'] ?>">
                        <input type="hidden" value="<?php echo $key['id'] ?>" name="id">
                        <input type="hidden" value="<?php echo $statusValue ?>" name="status">
                        <button type="submit" name="btnHapus" class="label label-danger"><span class="<?php echo $classBtn ?>"></span></button>
                      </form>
                    </td>
          </tr>


        <?php endforeach ?>

                   </table>
<br/>
  


                </div><!-- /.box-body -->
              </div><!-- /.box -->
             <div class="small-box bg-red">


           <div class="inner">
                  <center>
              <h4><br/>Insert User</h4>
             </center>
          </div>
          <a href="<?php echo Yii::app()->request->baseUrl; ?>/manageuser/input" class="small-box-footer">Search Again <i class="fa fa-arrow-circle-right"></i></a>
          </div>




            </div>
  </div>



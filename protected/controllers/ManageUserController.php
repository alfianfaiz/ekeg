<?php

class ManageUserController extends Controller
{
	public $layout = "mainlayout";
	/**
	 * Declares defualt home for index
	 */

	public function actionIndex()
	{
		
			    $connection = Yii::app()->db;
				$sql = "SELECT  * FROM user ";
				$command = $connection->createCommand($sql);
				$command->bindParam(':STATUS',$_POST['status'],PDO::PARAM_STR);
				$command->bindParam(':ID',$_POST['id'],PDO::PARAM_STR);
			
				$result=$command->queryAll();
				$this->render("index",array("result"=>$result));
	}
		public function actionEditUser(){
		if($_POST){
			Yii::app()->user->returnUrl = Yii::app()->request->urlReferrer;
			$dataUser = User::model()->find('id=:id',array(':id'=>$_POST['id']));
			$this->render('formedituser',array('dataUser'=>$dataUser));
		}
	}

	public function actionUpdateUser(){
		if($_POST){
			$user = User::model()->find('id=:id',array(':id'=>$_POST['id']));
			
			
			$user->nama = $_POST['username'];
			$user->password = $_POST['password'];
			$user->level = $_POST['level'];

			if($user->validate()){
				$user->save();
				
				$this->redirect(array('index'));
			} else {
				echo "gagal";
				// Yii::app()->user->setFlash('error','Maaf, simpan Program gagal. Mohon periksa kembali data yang anda inputkan');
				// $this->redirect(array('/errPage/errDB'));
			}
		}
	}



	public function actionHapusUser(){

		if ($_POST) {
			$connection = Yii::app()->db;
			$sql = "UPDATE `user` SET `status` = :STATUS WHERE `id` = :ID";
			$command = $connection->createCommand($sql);
			$command->bindParam(':STATUS',$_POST['status'],PDO::PARAM_STR);
			$command->bindParam(':ID',$_POST['id'],PDO::PARAM_STR);

			if($command->execute()){
				Yii::app()->user->setFlash("success","Operasi Hapus Berhasil !");
				$this->redirect(array('/errPage/errDB'));
			} else {
				Yii::app()->user->returnUrl = Yii::app()->request->urlReferrer;
				$this->redirect(array('/errPage/errDB'));
			}
			//SumberDana::model()->updateByPk($_POST['id'],'status',$criteria->condition,$criteria->params);
			//$this->redirect(array('index'));
		}
	}


	public function actionInput()
	{

			$this->render("input");

	}



	public function actionInsertUser(){
		if($_POST){
			$user = new User;
					
			$user->nama = $_POST['nama'];
			$user->password = md5($_POST['password']);
			$user->nip = $_POST['nip'];
			$user->username = $_POST['username'];
			$user->unitbagian = $_POST['unitbagian'];
			$user->tlp = $_POST['tlp'];
			$user->level = $_POST['level'];
		

			if($user->validate()){
				$user->save();
				$this->redirect(array('index'));
			} else {
				echo "gagal";
				// Yii::app()->user->setFlash('error','Maaf, simpan Program gagal. Mohon periksa kembali data yang anda inputkan');
				// $this->redirect(array('/errPage/errDB'));
			}
		}
	}





}

